import { Moment } from 'moment';

export interface IBook {
  id?: number;
  title?: string;
  creationDate?: Moment;
  coverImageContentType?: string;
  coverImage?: any;
}

export class Book implements IBook {
  constructor(
    public id?: number,
    public title?: string,
    public creationDate?: Moment,
    public coverImageContentType?: string,
    public coverImage?: any
  ) {}
}
